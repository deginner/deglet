## Local setup

1. `npm install`
2. `cd bitcore-wallet-api && npm install`
3. `make all`
4. Run `node e.js` and then visit `http://localhost:8118`.

deglet.xyz servers are used by default. In order to run local servers, adjust `app/src/constant/ServerConstants.js`, run `make`, and follow the instructions in the next section. Remember to install `grunt` using `npm install -g grunt-cli` in case it is not available.

This is known to work under node 0.10.x and iojs 2.5, but the latter is required for `npm test`.


## Server-side component

This client will communicate with a server, and `/misc` contains a demo server that provides what is expected. First install `redis` and `rethinkdb` servers, and then:

0. `sudo apt-get install git libffi-dev libssl-dev python-dev`
1. `cd misc`
2. `make`
3. `supervisord`

Step 2 will install Python packages and it's recommended to use virtualenv, otherwise sudo will be necessary. Step 3 will start the api server, incoming server (for handling incoming transactions regarding invoices and conversions), a local cosigning server, and other auxiliary servers.

Lastly, to receive incoming transactions for invoices and conversions it's necessary to configure your `bitcoin.conf` to use `misc/publi.sh` in `walletnotify` similar to:

`walletnotify=sh /home/user/deglet/misc/publi.sh %s`


## Running selenium tests

Dependencies to execute the selenium tests are not installed by default. So, if you want to run and/or contribute to selenium tests first you need to:

1. `npm install selenium-webdriver chai@2.3.0 chai-webdriver chai-as-promised`
2. `npm install -g mocha`
3. Copy `app/__tests__/selenium/data.js.orig` to `app/__tests__/selenium/data.js` and adjust it to list an user you control.

Afterwards these tests can be executed with `npm run-script selenium`.
