"use strict";

var Cookies = require('cookies');
var express = require('express');
var path = require('path');
if (process.env.USE_SSR) {
  /* You will also need NODE_PATH='.' */
  require('babel/register');    // Accept loading jsx files.
  var ssr = require('./ssr.jsx');
}

var app = express();
var port = 8118;


function hasUser(req, res) {
  var cookies = new Cookies(req, res);
  return cookies.get('user');
}


function getLanguage(req, res) {
  var cookies = new Cookies(req, res);
  return cookies.get('lang') || 'en';
}


app.use('/', express.static(path.join(__dirname, '/public')));

app.get('/favicon.ico', function(req, res) {
  res.send('');
});

app.get('/*', function(req, res) {
  if (process.env.USE_SSR) {
    if (req.path.match(/\/wallet$/) || hasUser(req, res)) {
      res.sendFile(path.join(__dirname, '/public/html/index.html'));
    } else {
      var lang = getLanguage(req, res);
      ssr(req, res, lang);
    }
  } else {
    res.sendFile(path.join(__dirname, '/public/html/index.html'));
  }
});

app.listen(port);
console.log('listening on port', port);
