"use strict";

/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    sass: {
      dist: {
        files: {
          'app/style/main.css': 'app/style/main.scss',
          'app/style/invoice.css': 'app/style/invoice.scss',
          'app/style/ticker.css': 'app/style/ticker.scss',
          'app/style/balance.css': 'app/style/balance.scss',
          'app/style/tx.css': 'app/style/tx.scss',
          'app/style/logo.css': 'app/style/logo.scss',
          'app/style/media.css': 'app/style/media.scss',
          'app/style/ladda.css': 'app/style/ladda.scss',
          'app/style/slidemenu.css': 'app/style/slidemenu.scss',
          'app/style/external.css': 'app/style/external.scss',
          'app/style/user/main.css': 'app/style/user/main.scss',
          'app/style/user/dropdown.css': 'app/style/user/dropdown.scss',
          'app/style/user/modal.css': 'app/style/user/modal.scss'
        }
      }
    },
    csslint: {
      options: {
        csslintrc: '.csslintrc'
      },
      src: ['app/style/**/*.css']
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      css: {
        src: [
              'app/style/invoice.css',
              'app/style/ticker.css',
              'app/style/balance.css',
              'app/style/normalize.css',
              'app/style/main.css',
              'app/style/tx.css',
              'app/style/logo.css',
              'app/style/user/*.css',
              'app/style/fontello.css',
              'app/style/fontello-anim.css',
              /* List files for external elements starting here. */
              'app/style/ladda.css',
              'app/style/slidemenu.css',
              'app/style/external.css',
              /* Styles that may impact all previous ones. */
              'app/style/media.css'],
        dest: 'public/css/app.css'
      }
    },
    cssmin: {
      css: {
        src: '<%= concat.css.dest %>',
        dest: 'public/css/<%= pkg.name %>.min.css'
      }
    },
    eslint: {
      app: {
        src: ['app/**/*{.js,.jsx}', 'misc/*.js', 'slideout/*.js', '!app/__tests__/casper/*']
      },
      bwapi: {
        src: ['bitcore-wallet-api/*.js', 'bitcore-wallet-api/lib/*.js']
      },
      grunt: {
        src: ['Gruntfile.js']
      }
    },
    browserify: {
      dist: {
        src: '<%= pkg.main %>',
        dest: 'public/js/app.dist.js',
        options: {
          transform: ['envify', 'reactify'],
          external: ['bitcore-wallet-api']
        }
      },
      dev: {
        files: {
          'public/js/app.js': ['<%= browserify.dist.src %>']
        },
        options: {
          watch: true,
          transform: ['reactify'],
          external: '<%= browserify.dist.options.external %>',
          browserifyOptions: {
            debug: true,
            fullPaths: true
          },
          watchifyOptions: {
            poll: 125
          }
        }
      },
      /* Bundle the bitcore-wallet-api in a separate js file. */
      bwapi: {
        src: './bitcore-wallet-api/index.js',
        dest: 'public/js/bwapi.dist.js',
        options: {
          alias: ['./bitcore-wallet-api/index.js:bitcore-wallet-api']
        }
      }
    },
    uglify: {
      build: {
        src: '<%= browserify.dist.dest %>',
        dest: 'public/js/app.min.js'
      },
      bwapi: {
        src: '<%= browserify.bwapi.dest %>',
        dest: 'public/js/bwapi.min.js'
      }
    },
    watch: {
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['eslint:grunt']
      },
      css: {
        files: '<%= concat.css.src %>',
        tasks: ['concat']
      },
      sass: {
        files: ['app/style/**/*.scss'],
        tasks: ['sass']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Build the app in release mode.
  grunt.registerTask('default', ['eslint',
                                 'browserify:dist', 'browserify:bwapi', 'uglify',
                                 'sass', 'csslint', 'concat', 'cssmin']);

  // Build and watch. Warning: running eslint is causing issues to the
  // watch task, which does not update correctly when both are in place.
  grunt.registerTask('dev', ['browserify:bwapi', 'uglify:bwapi',
                             'browserify:dev',
                             'sass', 'csslint', 'concat', 'cssmin',
                             'watch']);
};
