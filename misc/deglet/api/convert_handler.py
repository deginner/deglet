import sys
import traceback
from decimal import Decimal

import bitcoin
import bitcoin.rpc
import bitcoin.wallet
import redis
import rethinkdb as r
import sqlite3
from rethinkdb.errors import RqlDriverError
from flask import Blueprint, current_app, g
from flask_restful import Api, Resource, reqparse
from flask.ext.httpauth import HTTPBasicAuth

from .auth import get_password
from .error import Error
from ..common.bitcoin_wrapper import watch_address, get_address, send_to
from ..common.transaction import TransactionStatus, update_status, update_transaction
from ..common.generate_id import gen_id
from ..common import config


VALIDFOR = config.TRANSACTION['validfor']
TRANSACTION_LIMIT = config.TRANSACTION['limit']
RDB_TABLE = config.RDB_TABLE

CURRENCIES = ('BTC', 'USD')
FACTOR = {
    'BTC': Decimal('1e8'),
    'USD': Decimal('1e2')
}

if config.BITCOIN_TESTNET:
    bitcoin.params = bitcoin.TestNetParams()

convert_parser = reqparse.RequestParser()
convert_parser.add_argument('uid', type=str, trim=True, required=True)
convert_parser.add_argument('side', type=str, trim=True, required=True)
convert_parser.add_argument('amount', type=str, trim=True, required=True)
convert_parser.add_argument('currency', type=str, trim=True, required=True)
convert_parser.add_argument('otherCurrency', type=str, trim=True, required=True)
convert_parser.add_argument('address', type=str, trim=True, required=False)

auth = HTTPBasicAuth()
auth.get_password(get_password)

red = redis.StrictRedis()


def before_request():
    g.bitcoin = bitcoin.rpc.Proxy(service_url=config.BITCOIN_URL)

    try:
        g.rdb_conn = r.connect(**config.RDB)
    except RqlDriverError:
        pass

    g.db = sqlite3.connect(config.SQLDB_PATH)


def teardown_request(exception):
    g.db.commit()
    g.db.close()

    try:
        g.rdb_conn.close()
        del g.bitcoin
    except AttributeError:
        pass


def _user_sends(side, currency, other_currency):
    """
    Return True if user is responsible for sending from the wallet under
    its control, False otherwise.
    """
    sending = ((side == 'from' and currency == 'BTC') or
               (side == 'to' and other_currency == 'BTC'))
    return sending


def _conversion_rate(in_currency, pair):
    rate = red.hget('ticker:%s' % (pair[0] + pair[1]), 'last')
    rate = Decimal(rate)
    if in_currency != pair[0]:
        rate = 1 / rate

    return rate


def _calc_amount(side_amount, side_currency, other_currency, rate):
    """
    One side is known, calculate the other.
    """
    amount = Decimal(side_amount) / FACTOR[side_currency]
    other_amount = int(amount * rate * FACTOR[other_currency])
    return other_amount


def _start_trans(uid, side, amount, currency, other_currency, address):
    pair = ['BTC', 'USD']
    rate = _conversion_rate(currency, pair)
    other_amount = _calc_amount(amount, currency, other_currency, rate)

    if side == 'from':
        in_amount = amount
        in_currency = currency
        out_amount = other_amount
        out_currency = other_currency
    else:
        in_amount = other_amount
        in_currency = other_currency
        out_amount = amount
        out_currency = currency

    now = r.now()
    # XXX invoice_handler has very similar content for this.
    data = {
        'id': gen_id(),
        'owner_id': uid,  # XXX Not checked.
        'last_update': now,
        'payment_address': address,
        'input': {
            'amount': in_amount,
            'currency': in_currency,
        },
        'output': {
            'amount': out_amount,
            'currency': out_currency
        },
        'conversion': {
            'rate': '%.2f' % _conversion_rate(pair[0], pair),
            'reference_id': None
        },
        'status_info': [
            {
                'status': TransactionStatus.PENDING,
                'date': now,
                'expireAt': now + config.TRANSACTION['duration']
            },
            {
                'status': TransactionStatus.WAITING_USER,
                'date': now,
                'expireAt': now + VALIDFOR
            }
        ],
        'short_description': 'Conversion'
    }

    sending = _user_sends(side, currency, other_currency)
    if sending:
        # User will send a transaction.
        data['input']['sent'] = {'total': 0, 'tx': []}
    else:
        # User will receive a transaction.
        data['output']['received'] = {'total': 0, 'tx': []}

    # Update this transaction on the next expiration date.
    red.set(data['id'], 1, ex=VALIDFOR + 2)

    return data


class ConversionCreate(Resource):

    def _validate(self, args):
        # Validate input.
        side = args['side'].lower()[:10]
        if side not in ('from', 'to'):
            return Error.UnknownSide

        currency = args['currency'].upper()[:5]
        other_currency = args['otherCurrency'].upper()[:5]
        if currency not in CURRENCIES or other_currency not in CURRENCIES:
            return Error.UnknownCurrency
        elif currency == other_currency:
            return Error.NoConversion

        amount = Decimal(args['amount'])
        if currency == 'BTC':
            units = int(amount * Decimal('1e8'))
        else:
            units = int(amount * Decimal('1e2'))

        if units < TRANSACTION_LIMIT[currency]['minimum']:
            return Error.TooLow

        sending = _user_sends(side, currency, other_currency)
        if sending:
            # This user is sending to an external provider
            # to an address controled by the provider.
            address = get_address(g.bitcoin)
            addy = {'address': address, 'watch': False}
        else:
            # This user will receive from an external provider
            # to an address controled by the user.
            address = args['address']
            addy = {'address': address, 'watch': True}

        try:
            bitcoin.wallet.CBitcoinAddress(address)
        except Exception:
            traceback.print_exc(file=sys.stderr)
            return Error.InvalidAddress

        if r.table(RDB_TABLE)['payment_address'].count(address).run(g.rdb_conn):
            # XXX Could limit this to active states instead.
            # (waiting_user or pending)
            return Error.AddressInUse

        return side, units, currency, other_currency, addy

    def post(self):
        args = convert_parser.parse_args(strict=True)
        uid = args['uid']
        validated = self._validate(args)

        if isinstance(validated, dict):
            # Got an error, "validated" is actually an Error namedtuple.
            return validated

        side, units, currency, other_currency, addy = validated
        address = addy['address']

        if addy['watch']:
            # Start watching the address.
            watch_address(address, g.bitcoin)

        # Create initial transaction.
        data = _start_trans(uid, side, units, currency, other_currency, address)

        # If converting from USD to BTC,
        # check if this user USD balance >= input_amount.
        if 'received' in data['output']:
            assert data['input']['currency'] == 'USD', data['input']
            usdamount = data['input']['amount']
            res = g.db.execute("SELECT 1 FROM walletblob WHERE uid = ? AND usdbalance >= ?",
                               (uid, usdamount)).fetchone()
            if not res:
                return Error.LowBalance

        # The balance for performing a BTC to USD conversion is
        # checked in BWS.

        result = r.table(RDB_TABLE).insert(data).run(g.rdb_conn)
        assert result['inserted'] == 1
        assert sum(result.values()) == 1

        record = r.table(RDB_TABLE).get(data['id']).run(g.rdb_conn,
                                                        time_format='raw')
        return record


class ConversionPay(Resource):

    decorators = [auth.login_required]

    def _update_nosql(self, txid, out_amount, trans):
        # Update transaction in nosql db.
        tid = trans['id']
        update = {
            'output': {
                'received': {
                    'total': out_amount,
                    'tx': [txid]
                }
            }
        }
        update_transaction(tid, update)

        # Update its status now.
        trans = r.table(RDB_TABLE).get(tid).run(g.rdb_conn, time_format='raw')
        new_status = update_status(trans)
        return new_status

    def post(self, tid):
        uid = auth.username()
        trans = r.table(RDB_TABLE).get(tid).run(g.rdb_conn,
                                                time_format='raw')
        if not trans:
            return Error.UnknownID

        if 'received' in trans['output']:
            assert trans['input']['currency'] == 'USD', trans['input']
            usdamount = trans['input']['amount']
            res = g.db.execute("SELECT usdbalance FROM walletblob WHERE uid = ? AND usdbalance >= ?",
                               (uid, usdamount)).fetchone()
            if not res:
                return Error.LowBalance
        else:
            return Error.InvalidBTCUSDPayment

        # Pay transaction.
        g.db.execute("INSERT INTO usdhistory (uid, amount, tid) VALUES (?, ?, ?)",
                     (uid, -usdamount, tid))
        good = g.db.execute("SELECT 1 FROM walletblob WHERE uid = ? AND usdbalance = ?",
                            (uid, res[0] - usdamount)).fetchone()
        if not good:
            return Error.InvalidUserState

        try:
            # Send bitcoins.
            out_amount = trans['output']['amount']
            txid = send_to(trans['payment_address'], out_amount, g.bitcoin)
        except Exception as err:
            g.db.rollback()
            current_app.logger.exception(err)
            return Error.SendFail

        g.db.commit()

        try:
            new_status = self._update_nosql(txid, out_amount, trans)
        except Exception as err:
            current_app.logger.exception(err)
            return Error.StateUpdateFail

        return {'id': tid, 'status': new_status}


api_blueprint = Blueprint('convert', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(ConversionCreate, '/test/conversion')
api.add_resource(ConversionPay, '/test/conversion/<string:tid>')
