import rethinkdb as r
from rethinkdb.errors import RqlDriverError
from flask import Blueprint, g
from flask_restful import Api, Resource
from flask.ext.httpauth import HTTPBasicAuth

from .auth import get_password
from ..common.config import RDB, RDB_TABLE


auth = HTTPBasicAuth()
auth.get_password(get_password)


def before_request():
    try:
        g.rdb_conn = r.connect(**RDB)
    except RqlDriverError:
        pass


def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


class Address(Resource):

    decorators = [auth.login_required]

    def get(self, address):
        uid = auth.username()

        query = r.table(RDB_TABLE).get_all(address, index='payment_address')
        query = query.filter(r.row['owner_id'].match(uid))
        total = query.count().run(g.rdb_conn)

        result = {
            'address': address
        }
        if total > 0:
            result['uid'] = uid

        return result


api_blueprint = Blueprint('address', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(Address, '/test/address/<string:address>')
