import sys
import traceback
from decimal import Decimal

import bitcoin
import bitcoin.rpc
import bitcoin.wallet
import redis
import rethinkdb as r
from rethinkdb.errors import RqlDriverError
from flask import Blueprint, g
from flask_restful import Api, Resource, reqparse, abort

from .error import Error
from ..common.bitcoin_wrapper import get_address, watch_address
from ..common.transaction import TransactionStatus, update_waitinguser, update_status
from ..common.generate_id import valid_id, gen_id
from ..common import config


TRANSACTION_LIMIT = config.TRANSACTION['limit']
VALIDFOR = config.TRANSACTION['validfor']
RDB_TABLE = config.RDB_TABLE

TITLE_MAXLEN = 22

if config.BITCOIN_TESTNET:
    bitcoin.params = bitcoin.TestNetParams()

invoice_parser = reqparse.RequestParser()
invoice_parser.add_argument('uid', type=str, trim=True, required=True)
invoice_parser.add_argument('amount', type=str, trim=True, required=True)
invoice_parser.add_argument('currency', type=str, trim=True, required=True)
invoice_parser.add_argument('address', type=str, trim=True, required=False)
invoice_parser.add_argument('title', type=str, trim=True, required=False)

red = redis.StrictRedis()


def before_request():
    g.bitcoin = bitcoin.rpc.Proxy(service_url=config.BITCOIN_URL)
    try:
        g.rdb_conn = r.connect(**config.RDB)
    except RqlDriverError:
        pass


def teardown_request(exception):
    del g.bitcoin
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


def _start_invoice(uid, address, amount, currency, title='Invoice'):
    now = r.now()

    data = {
        'id': gen_id(),
        'owner_id': uid,  # XXX Not checked.
        'last_update': now,
        'payment_address': address,
        'input': {        # Input will be calculated later.
            'amount': None,
            'currency': 'BTC',
            'received': {'total': 0, 'tx': []}
        },
        'output': {
            'amount': amount,
            'currency': currency
        },
        'conversion': {   # Will be calculated later if necessary.
            'rate': None,
            'reference_id': None
        },
        'status_info': [{
            'status': TransactionStatus.WAITING_USER,
            'date': now,
            'expireAt': now + VALIDFOR
        }],
        'short_description': title
    }

    if currency == 'BTC':
        data['conversion']['rate'] = 1
        data['input']['amount'] = data['output']['amount']

    return data


class InvoiceCreate(Resource):

    def _validate(self, invoice):
        # Validate input.
        currency = invoice['currency'].upper()[:5]
        if currency not in ('BTC', 'USD'):
            return Error.UnknowCurrency

        amount = Decimal(invoice['amount'])
        if currency == 'BTC':
            units = int(amount * Decimal('1e8'))
            # Since this does not involve a conversion, the user
            # also provides an address she owns.
            address = invoice['address']
            addy = {'address': address, 'watch': True}
        else:
            units = int(amount * Decimal('1e2'))

        if units < TRANSACTION_LIMIT[currency]['minimum']:
            return Error.TooLow

        if currency != 'BTC':
            # The address is provided by a provider when
            # there is a conversion.
            address = get_address(g.bitcoin)
            addy = {'address': address, 'watch': False}

        try:
            bitcoin.wallet.CBitcoinAddress(address)
        except Exception:
            traceback.print_exc(file=sys.stderr)
            return Error.InvalidAddress

        if r.table(RDB_TABLE)['payment_address'].count(address).run(g.rdb_conn):
            # XXX Could limit this to active states instead.
            # (waiting_user or pending)
            return Error.AddressInUse

        return currency, units, addy

    def post(self):
        invoice = invoice_parser.parse_args(strict=True)
        validated = self._validate(invoice)

        if isinstance(validated, dict):
            # Got an error.
            return validated

        currency, units, addy = validated
        uid = invoice['uid']
        title = (invoice['title'] or 'Invoice')[:TITLE_MAXLEN]

        if addy['watch']:
            # Start watching the address.
            watch_address(addy['address'], g.bitcoin)

        # Create initial transaction.
        data = _start_invoice(uid, addy['address'], units, currency, title)
        result = r.table(RDB_TABLE).insert(data).run(g.rdb_conn)
        assert result['inserted'] == 1
        assert sum(result.values()) == 1

        record = r.table(RDB_TABLE).get(data['id']).run(g.rdb_conn,
                                                        time_format='raw')

        # Update this transaction on the next expiration date.
        red.set(data['id'], 1, ex=VALIDFOR + 2)

        return record


class InvoiceLoad(Resource):

    def _get_invoice(self, iid):
        # Get invoice information.
        iid = iid.upper()
        if not valid_id(iid):
            abort(403, message=Error.InvalidID['error'])
            return

        data = r.table(RDB_TABLE).get(iid)
        data = data.run(g.rdb_conn, time_format='raw')

        if data is None:
            abort(403, message=Error.UnknownID['error'])
            return

        return data

    def get(self, iid):
        invoice = self._get_invoice(iid)
        if not invoice:
            return

        if update_status(invoice) is not None:
            # Invoice updated, need to load again.
            invoice = self._get_invoice(iid)

        return invoice

    def post(self, iid):
        invoice = self._get_invoice(iid)
        if not invoice:
            return

        if invoice['status_info'][0]['status'] != TransactionStatus.WAITING_USER:
            return Error.Alreadystarted

        update_waitinguser(invoice)

        # Return modified record.
        iid = invoice['id']
        record = r.table(RDB_TABLE).get(iid).run(g.rdb_conn, time_format='raw')
        return record


api_blueprint = Blueprint('invoice', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(InvoiceLoad, '/test/invoice/<string:iid>')
api.add_resource(InvoiceCreate, '/test/invoice')


if __name__ == "__main__":
    # Simple check.
    btc = bitcoin.rpc.Proxy(service_url=config.BITCOIN_URL)
    address = get_address(btc)
    print bitcoin.wallet.CBitcoinAddress(address)
