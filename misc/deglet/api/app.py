from flask.ext.cors import CORS

from ..common import base_server
from . import auth_handler, ticker_handler, history_handler
from . import invoice_handler, convert_handler, address_handler


def build_app():
    app = base_server.create(auth_handler, ticker_handler, history_handler,
                             invoice_handler, convert_handler, address_handler)

    CORS(app)

    app.debug = False
    return app
