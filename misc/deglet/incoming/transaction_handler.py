import json

import rethinkdb as r
from rethinkdb.errors import RqlDriverError
from flask import Blueprint, g
from flask_restful import Api, Resource, reqparse

from ..common.transaction import TransactionStatus, update_pending
from ..common.config import RDB, RDB_TABLE


parser = reqparse.RequestParser()
parser.add_argument('raw', type=str, required=True)


def before_request():
    try:
        g.rdb_conn = r.connect(**RDB)
    except RqlDriverError:
        pass


def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


def _collect_transactions(btctrans):
    data = []
    print btctrans
    for detail in btctrans['details']:
        if detail['category'] != 'receive':
            continue

        # Check if this address belongs to a invoice or conversion.
        query = {'payment_address': detail['address']}
        transaction = list(r.table(RDB_TABLE).filter(query).run(
            g.rdb_conn, time_format='raw'))
        if not transaction:
            continue
        assert len(transaction) == 1

        trans = transaction[0]
        if trans['status_info'][0]['status'] != TransactionStatus.PENDING:
            # Transaction is not ready for receiving payments.
            continue

        amount = int(round(detail['amount'] * 1e8))
        data.append((trans, amount))

    return data


def _update_transactions(data, txid):
    for trans, new_amount in data:
        if update_pending(trans) is not None:
            continue

        if 'received' in trans['input']:
            key = 'received'
        else:
            key = 'sent'

        txset = trans['input'][key]['tx']
        txset.append(txid)
        total_amount = trans['input'][key]['total'] + new_amount

        res = r.table(RDB_TABLE).get(trans['id']).update({
            'last_update': r.now(),
            'input': {
                key: {
                    'total': total_amount,
                    'tx': list(set(txset))
                }
            }
        }).run(g.rdb_conn)
        print res, trans['id']

    for trans, _ in data:
        # XXX This could raise an AssertionError.
        update_pending(trans)


class TransactionReceive(Resource):

    def post(self):
        raw = parser.parse_args()['raw']
        btctrans = json.loads(raw)

        txid = btctrans['txid']
        query = lambda entry: entry['input']['received']['tx'].contains(txid)
        if r.table(RDB_TABLE).filter(query).count().run(g.rdb_conn):
            return {'error': 'already received'}

        data = _collect_transactions(btctrans)
        _update_transactions(data, txid)

        return {"ok": txid}


api_blueprint = Blueprint('transaction', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(TransactionReceive, '/transaction/receive')
