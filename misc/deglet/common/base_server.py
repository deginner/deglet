import logging

from flask import Flask
from flask_restful import Api


def create(*modules):
    app = Flask(__name__)
    app.url_map.strict_slashes = False

    api = Api(app, catch_all_404s=True)
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.INFO)
    app.logger.addHandler(log_handler)

    for module in modules:
        blueprint = getattr(module, 'api_blueprint')
        app.register_blueprint(blueprint)

    return app
