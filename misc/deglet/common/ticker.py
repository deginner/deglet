from decimal import Decimal


def calc_change(data):
    last = Decimal(data['last'])
    day = Decimal(data['day'])
    month = Decimal(data['month'])
    change24h = last - day
    changeMonth = last - month

    data['change'] = {
        'day': {
            'amount': '%.2f' % change24h,
            'pct': '%.2f' % ((change24h / day) * 100)
        },
        'month': {
            'amount': '%.2f' % changeMonth,
            'pct': '%.2f' % ((changeMonth / month) * 100)
        }
    }

    return data
