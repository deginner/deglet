from bitcoin.core import b2lx


def watch_address(address, client):
    btcresult = client.importaddress(address, "", False)
    assert btcresult is None, btcresult


def get_address(client):
    btcresult = client.getnewaddress()
    assert btcresult is not None, btcresult
    return str(btcresult)


def send_to(address, amount, client):
    btcresult = client.sendtoaddress(address, amount)
    assert btcresult is not None, btcresult
    return b2lx(btcresult)
