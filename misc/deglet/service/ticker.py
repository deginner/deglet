import json
import time
from decimal import Decimal
from datetime import datetime

import redis
import requests

from ..common.ticker import calc_change


# XXX Example
PAIR = 'BTCUSD'
URL = 'https://api.bitfinex.com/v1/pubticker/'


def get_ticker(pair):
    try:
        res = requests.get(URL + pair, timeout=5)
        value = '%.2f' % Decimal(res.json()['mid'])
        return value
    except Exception, err:
        print 'request failed @ %s [%s]' % (time.ctime(), err)


def setup(pair=PAIR):
    red = redis.StrictRedis()
    ticker_key = 'ticker:%s' % pair

    store = red.hgetall(ticker_key)
    if 'day' not in store or 'month' not in store:
        ticker = get_ticker(pair)
        if not ticker:
            raise Exception('ticker setup failed')

        # If 'day' or 'month' are not available, set them now.
        # It's better to define a correct base value for the day and month
        # as that is used to display 24h/month change to the user.
        if 'day' not in store:
            red.hset(ticker_key, 'day', ticker)
        if 'month' not in store:
            red.hset(ticker_key, 'month', ticker)


def main(pair=PAIR):
    ticker_key = 'ticker:%s' % pair
    red = redis.StrictRedis()

    dateNow = datetime.now()
    while True:
        timeNow = time.time()
        value = get_ticker(pair)
        if value is None:
            time.sleep(1)
            continue

        # Check for new reference and update as needed.
        newDate = datetime.now()
        if newDate.day != dateNow.day:
            print 'Got new day reference @ %s' % time.ctime()
            red.hset(ticker_key, 'day', value)
            dateNow = newDate
        if newDate.month != dateNow.month:
            print 'Got new month reference @ %s' % time.ctime()
            red.hset(ticker_key, 'month', value)
            dateNow = newDate

        # Save last ticker too.
        current_last = red.hget(ticker_key, 'last')
        red.hset(ticker_key, 'last', value)

        # Publish ticker.
        if current_last != value:
            data = red.hgetall(ticker_key)
            data['pair'] = pair
            red.publish('ticker', json.dumps(calc_change(data)))

        sleep = max(0, 1 - (time.time() - timeNow))
        print value, sleep
        time.sleep(sleep)


if __name__ == "__main__":
    setup()
    main()
