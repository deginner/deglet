import sys
import json
import time

import redis
import sqlite3
import rethinkdb as r

from ..common.config import SQLDB_PATH, RDB, RDB_TABLE


def conn():
    return {
        'redis': redis.StrictRedis(),
        'sql': sqlite3.connect(SQLDB_PATH),
        'rdb': r.connect(**RDB)
    }


def watch():
    c = conn()
    stream = r.table(RDB_TABLE).changes(include_states=True).run(c['rdb'])
    for change in stream:
        print time.ctime(), change
        if change.get('old_val') is None:
            continue

        transaction = change['new_val']
        update(c, transaction)


def update(connection, trans):
    sql = connection['sql']

    uid = trans['owner_id']
    status = trans['status_info'][0]['status']
    if status != 'paid':
        return

    if trans['output']['currency'] == 'USD':
        # Update balance when the output is in USD.
        print 'updating balance for %s' % uid
        values = (uid, trans['output']['amount'], trans['id'])
        sql.execute("INSERT INTO usdhistory (uid, amount, tid) "
                    "VALUES(?, ?, ?)", values)
        sql.commit()
        # XXX Ping some server: "ready to hedge".

    res = sql.execute("SELECT usdbalance FROM walletblob WHERE uid = ?",
                      (uid, )).fetchone()
    if res:
        # Publish new balance.
        data = {'uid': uid, 'total': res[0], 'currency': 'USD'}
        connection['redis'].publish('fiat-balance', json.dumps(data))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        watch()
    else:
        # Update a specific transaction.
        connection = conn()
        tid = sys.argv[1].upper()
        trans = r.table(RDB_TABLE).get(tid).run(connection['rdb'])
        print trans
        if trans:
            update(connection, trans)
