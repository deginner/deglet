"use strict";

var React = require('react');
var Link = require('react-router').Link;
var walletLink = require('../../src/constant/AppLinks').name.wallet;


function template() {

  return (
    <div>
      <p>{this.getIntlMessage('page_notfound.message')}</p>
      <Link to={walletLink}>{this.getIntlMessage('page_notfound.wallet_link')}</Link>
    </div>
  );

}


module.exports = template;
