/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var walletLink = require('../../src/constant/AppLinks').name.wallet;


function trim(s) {
  /* Remove trailing zeros from s, and then the remaining period
   * if there is one. */
  if (!s) {
    return s;
  }

  return (s.toString()).replace(/0+$/, '').replace(/\.$/, '');
}


function formatOutput(i18n, vout) {
  return (
    <div className="tx-output">
      <p className="tx-info-title">{i18n('widget.txdetail.outputs')}</p>
      {vout.map(
        function(entry) {
          return (
            <div className="tx-vout" key={"vout-" + entry.n}>
              <div className="row">

                <div className="col-xs-8">
                  <span className="tx-address">
                    {entry.scriptPubKey ? entry.scriptPubKey.addresses : "unknown"}
                  </span>
                </div>

                <div className="col-xs-4">
                  <span className="tx-amount">{trim(entry.value)} BTC</span> {entry.spentTxId ?
                    <span className="tx-spent">
                      <Link to="txdetail" params={{txid: entry.spentTxId}}>({i18n('widget.txdetail.spent')})</Link>
                    </span> :
                    null
                  }
                </div>

              </div>
            </div>
          );
        })
      }
    </div>
  );
}


function template() {
  var i18n = this.getIntlMessage;
  var data = this.state.data || {};
  var loading = this.state.loading;
  var fmtDate = this.formatDate;
  var fmtNumber = this.formatNumber;

  function format(tx) {
    if (loading) {
      return null;
    }

    var labelClass = "col-xs-6 col-sm-3 col-lg-2 tx-info-label";
    var valueClass = "col-xs-6 col-sm-9 col-lg-10 tx-info-value";

    return (
      <div>

        <div className="row middle-xs break-word">
          <div className="col-xs-12 col-sm-10 col-md-8 col-lg-6 sm-margin-top">
            {formatOutput(i18n, tx.vout || [])}
          </div>
        </div>

        <div className="row tx-info-extra">
          <div className={labelClass}>{i18n('generic.confs')}</div>
          <div className={valueClass}>{tx.confirmations ? fmtNumber(tx.confirmations) : 0}</div>

          <div className={labelClass}>{i18n('widget.txdetail.blocktime')}</div>
          <div className={valueClass}>
            {tx.blocktime ?
              fmtDate(tx.blocktime * 1000, 'withTime') :
              i18n('widget.txdetail.unconfirmed')
            }
          </div>

          <div className={labelClass}>{i18n('widget.txdetail.totaloutput')}</div>
          <div className={valueClass}>{trim(tx.valueOut)} BTC</div>
        </div>

      </div>
    );
  }

  return (
    <div className="tx-page">
      {this.state.loading ?
        <p className="break-word">
          {i18n('generic.loading')}
        </p> :
        <div className="tx-info">
          {!data.error ? format(data) : i18n(data.error)}
        </div>}

      <hr/>

      <p>
        <Link to={walletLink}>{i18n('page_notfound.wallet_link')}</Link>
      </p>
    </div>
  );

}


module.exports = template;
