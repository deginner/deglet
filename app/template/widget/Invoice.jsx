/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var LaddaButton = require('react-ladda');
var classNames = require('classnames');
var formatAmount = require('../helper').formatAmount;
var thisServer = require('../../src/constant/ServerConstants').THIS_URL;


function formatTransaction(component) {
  var trans = component.state.data;
  var obj = {
    active: false,              /* Countdown is not running. */
    loading: trans === null,    /* Transaction is being fetched. */
    invalidTrans: false,        /* Is the data received in an unknown format? */
    status: null,               /* Latest transaction status. */
    finished: component.state.timer.expired,
    empty: "—",
    shortDescr: null,
    createTime: null,
    amountToPay: null,
    amountPaid: null,
    goodUntil: null,
    amountToReceive: null,
    conversionRate: null,
    type: "invoice"
  };

  obj.amountToPay = obj.empty;
  obj.amountPaid = obj.empty;
  obj.amountToReceive = obj.empty;
  obj.conversionRate = obj.empty;

  if (!obj.loading && typeof trans.status_info === 'undefined') {
    obj.shortDescr = <i>{component.getIntlMessage('transaction.unknown')}</i>;
    obj.createTime = obj.empty;
    obj.invalidTrans = true;
    return obj;
  }

  if (!obj.loading) {
    obj.status = trans.status_info[0].status;
    obj.active = (obj.status === 'pending' && !obj.finished) ? true : false;
    if (!obj.active && obj.status !== 'waiting_user') {
      obj.finished = true;
    }

    /* Format the timestamps for regular humans. */
    var initialStatus = trans.status_info.slice(-1)[0];
    obj.createTime = component.formatDate(initialStatus.date.epoch_time * 1000, 'withTime');
    obj.goodUntil = component.formatTime(initialStatus.expireAt.epoch_time * 1000, 'hhmm');

    obj.shortDescr = trans.short_description;
    if (trans.output.amount !== null) {
      obj.amountToReceive = formatAmount(
        trans.output.amount,
        trans.output.currency);
    }
    if (trans.conversion.rate !== null) {
      if (trans.input.currency !== trans.output.currency) {
        obj.conversionRate = trans.conversion.rate + " " + trans.output.currency + "/" + trans.input.currency;
      } else {
        obj.conversionRate = "1 " + trans.output.currency + "/" + trans.input.currency;
      }
    }
    var received = 0;
    if (trans.input.received) {
      obj.type = 'invoice';
      received = trans.input.received.total;
    } else if (trans.input.sent) {
      /* A conversion where the user sends BTC and receives USD. */
      obj.type = 'conversion';
      received = trans.input.sent.total;
    } else {
      /* A conversion where the user sends USD and receives BTC.
       * In this case it's assumed that only 1 transaction is used
       * and "received" really means "paid". */
      obj.type = 'conversion';
      received = trans.output.received.total ? trans.input.amount : 0;
    }
    if (trans.input.amount !== null) {
      /* Note that amountToPay is always displayed as 0 after
       * the countdown expires or if the transaction has been paid. */
      obj.amountToPay = formatAmount(
        (obj.status === 'paid') ? 0 : trans.input.amount - received,
        trans.input.currency);
    }
    obj.amountPaid = formatAmount(received, trans.input.currency);
  } else {
    obj.createTime = obj.empty;
    if (component.state.error) {
      /* This transaction won't load, since there is an error. */
      obj.shortDescr = <i>{component.getIntlMessage(component.state.error)}</i>;
    } else {
      obj.shortDescr = component.getIntlMessage('generic.loading');
    }
  }

  if (obj.type === 'conversion') {
    obj.shortDescr = component.getIntlMessage('invoice.' + (obj.shortDescr.toLowerCase()));
  }

  return obj;
}


function transactionAddress(invoice, component) {
  var leftElem;
  var rightElem;

  if (invoice.finished) {
    /* Countdown finished or invoice got canceled. */
    var key = component.state.data.input.received ? "received" : "sent";
    var txlist;
    var txlinks;

    if (component.state.data.input[key]) {
      txlist = component.state.data.input[key].tx;
    } else if (component.state.data.output.received) {
      txlist = component.state.data.output.received.tx;
    }

    txlinks = txlist.map(function(tx) {
      return (
        <span key={tx} className="invoice-element-heavy invoice-element-secondary invoice-element-received">
          <Link to="txdetail" params={{txid: tx}} key={tx}>{tx}</Link>
        </span>
      );
    });

    rightElem = (
      <p className="invoice-element invoice-received">
        <span className="invoice-element-title">
          {key === "received" ?
            component.getIntlMessage('invoice.received_from') :
            component.getIntlMessage('invoice.sent_through')}
        </span>
        {txlinks.length > 0 ?
          <span className="invoice-received-list">{txlinks}</span> :
          <span className="invoice-element-heavy invoice-element-secondary invoice-element-received">
            {component.getIntlMessage('invoice.nowhere')}
          </span>
        }
      </p>
    );
  } else if (invoice.active && invoice.type === 'invoice') {
    /* Invoice is active, display the QR code. */
    var qr = component.state.qrShowUri ? 'qrUri' : 'qrAddress';
    rightElem = (
      <div className="invoice-qr">
        <img src={component.state[qr].img} title={component.state[qr].url} />
        <a href="#" onClick={component.toggleQr}>
          {qr === 'qrUri' ?
            component.getIntlMessage('invoice.qr_showaddress') :
            component.getIntlMessage('invoice.qr_showuri')}
        </a>
      </div>
    );
  }

  if (invoice.finished || invoice.active) {
    var addressKey = (invoice.type === 'invoice') ? 'payment_address' : 'recipient_address';
    leftElem = (
      <p className={"invoice-element invoice-address" + (invoice.active && invoice.type === 'invoice' ? " invoice-address-active" : "")}>
        <span className="invoice-element-title">
          {component.getIntlMessage('invoice.' + addressKey)}
        </span>
        <textarea className={"invoice-element-heavy" + (!invoice.active ? " invoice-element-secondary" : "")}
                  onMouseUp={component.selectElement}
                  value={component.state.data.payment_address} readOnly>
        </textarea>
      </p>
    );
  }

  return (
    <div>
      {leftElem}
      {rightElem}
    </div>
  );
}


function invoiceActivateButton(hasError, component) {
  /* Invoice is not active yet, show activation button. */
  var minutesDuration = parseInt(component.state.timer.duration / 60);
  var button = null;
  var text;

  if (!hasError) {
    // Button for activating the countdown.
    button = (
      <LaddaButton buttonStyle="zoom-in" loading={component.state.loadingRate}
                   type="button" onClick={component.onCalcRate}>
        {component.getIntlMessage('invoice.countdown_start')}
      </LaddaButton>
    );
    text = (
      <p className="invoice-cycle">
        {component.formatMessage(
          component.getIntlMessage('invoice.countdown_hint'),
          {duration: minutesDuration}
        )}
      </p>
    );
  } else {
    button = (
      <button type="button" onClick={component.onRetry}>
        {component.getIntlMessage('invoice.retry')}
      </button>
    );
    text = (
      <p className="invoice-cycle">
        {component.getIntlMessage('invoice.retry_hint')}
      </p>
    );
  }

  return (
    <div className="invoice-action invoice-action-large">
      {button}
      {text}
    </div>
  );
}


function invoicePayButton(invoice, component) {
  if (!invoice.finished && !component.state.timer.expired) {
    /* Countdown is running. */
    var status = component.state.payStatus;
    var isError = status.substring(0, 6) === 'error.';
    return (
      <div className="invoice-action">
        <LaddaButton buttonStyle="zoom-in" loading={!isError && status !== ''}
                     type="button" onClick={component.onPayTransaction}>
          {component.getIntlMessage('invoice.paynow')}
        </LaddaButton>
        <p className="invoice-cycle">
          {component.getIntlMessage(status)}
        </p>
      </div>
    );
  }

  return null;
}


function invoiceStatus(invoice, component) {
  var result = null;

  if (invoice.loading || invoice.invalidTrans) {
    return result;
  }

  if (!invoice.active && !invoice.finished) {
    /* Countdown didn't start yet. */
    if (invoice.goodUntil) {
      result = (
        <p className="invoice-gooduntil">
          {component.formatMessage(
            component.getIntlMessage('invoice.gooduntil'),
            {date: invoice.goodUntil}
          )}
        </p>
      );
    }
  } else if (!invoice.finished && !component.state.timer.expired) {
    /* Countdown is running. */
    var timeLeft = component.state.timer.left;
    var minutesLeft = parseInt(timeLeft / 60);
    var minsecLeft = timeLeft % 60;
    if (minsecLeft < 10) {
      minsecLeft = '0' + minsecLeft;
    }

    result = (
      <p className="invoice-element">
        <span className="invoice-element-title">{component.getIntlMessage('invoice.timeleft')}</span>
        <span className="invoice-element-heavy invoice-element-secondary">
          {minutesLeft + ":" + minsecLeft}
        </span>
      </p>
    );
  } else if (invoice.finished) {
    /* Countdown is no longer running. */
    var status = invoice.status;
    if (component.state.timer.expired) {
      /* Expired while the Invoice page was open. */
      status = component.formatMessage(
        component.getIntlMessage('invoice.expired_at'),
        {date: component.formatTime(component.state.timer.expireAt * 1000, 'hhmm')}
      );
    } else if (invoice.status === 'expired') {
      status = component.formatMessage(
        component.getIntlMessage('invoice.expired_at'),
        {date: component.formatTime(component.state.data.status_info[0].date.epoch_time * 1000, 'hhmm')}
      );
    } else {
      status = component.getIntlMessage('transaction.status.' + status);
    }

    result = (
      <p className="invoice-element invoice-status">
        <span className="invoice-element-title">
          {component.getIntlMessage('invoice.status')}
        </span>
        <span className="invoice-element-heavy invoice-element-secondary">
          {status}
        </span>
      </p>
    );
  }

  return result;
}



function template() {
  var bottom;
  var bottomStyle = {
    display: this.props.hideBottom ? "none" : ""
  };
  var invoice = formatTransaction(this);
  var hasError = this.state.error || invoice.invalidTrans;
  var qrUrl = null;
  var params = {
    logo: '../img/invoice_logo.png',
    paylogo: {
      img: '../img/bitcoin_pay.png',
      text: this.getIntlMessage('invoice.pay_with_bitcoin')
    }
  };

  if (invoice.active) {
    qrUrl = this.state.qrUri.url;
  }

  var content = (
    <div className="invoice">

      <div className="invoice-header">
        <img className="invoice-logo" src={params.logo} alt="" />
        <p className="invoice-element">
          <span className="invoice-element-title">{this.getIntlMessage('invoice.date')}</span>
          <span>{invoice.createTime}</span>
        </p>
        <div className="invoice-title">
          <h2 lang="en">{invoice.shortDescr}</h2>
        </div>
      </div>
      <hr/> {/* end header */}

      <div className="invoice-body">

        {/* Amount required to complete the invoice. */}
        <p className="invoice-element">
          <span className="invoice-element-title">
            {this.getIntlMessage('invoice.payment_amount')}
          </span>
          <span className={"invoice-element-heavy" + (invoice.finished ? " invoice-element-secondary" : "")}>
            {invoice.active ? invoice.amountToPay : invoice.empty}
          </span>
        </p>

        {!invoice.loading && transactionAddress(invoice, this)}
        {!invoice.loading && !invoice.active && !invoice.finished && invoiceActivateButton(hasError, this)}
        {invoice.type === 'conversion' && invoicePayButton(invoice, this)}

        <div className="invoice-main-bottom">
          {invoiceStatus(invoice, this)}
          <a href={qrUrl} className={!qrUrl ? "invoice-payinactive" : null}>
            <img src={params.paylogo.img} alt={params.paylogo.text} title={qrUrl}
              className={classNames("invoice-paymethod", {"invoice-payactive": qrUrl !== null})} />
          </a>
        </div>

        <div ref="defaultBar" className="invoice-bar invoice-default-bar"></div>
        <div ref="progressBar" className={classNames(
          "invoice-bar", "invoice-bar-" + invoice.status,
          {"invoice-progress-bar": invoice.status === 'pending'},
          {"invoice-bar-expired": this.state.timer.expired}
        )}></div>

      </div>
      <hr/> {/* end body */}

      <div className="invoice-footer">
        <table>
          <thead>
            <tr>
              <th colSpan="2">{this.getIntlMessage('generic.total')}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="invoice-element-title">{this.getIntlMessage('generic.due')}</td>
              <td className="invoice-element-title">{this.getIntlMessage('generic.paid')}</td>
            </tr>
            <tr>
              <td>{!invoice.loading ? invoice.amountToPay : invoice.empty}</td>
              <td>{!invoice.loading ? invoice.amountPaid : invoice.empty}</td>
            </tr>
          </tbody>
        </table>

        <table>
          <thead>
            <tr>
              <th colSpan="2">
                {invoice.type === "invoice" ?
                  this.getIntlMessage('generic.origin') :
                  this.getIntlMessage('generic.output')
                }
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="invoice-element-title">{this.getIntlMessage('generic.amount')}</td>
              <td className="invoice-element-title">{this.getIntlMessage('invoice.rate')}</td>
            </tr>
            <tr>
              <td>{!invoice.loading ? invoice.amountToReceive : invoice.empty}</td>
              <td>{!invoice.loading ? invoice.conversionRate : invoice.empty}</td>
            </tr>
          </tbody>
        </table>
      </div> {/* end footer */}

    </div>
  );

  bottom = (
    <div className="invoice-outside" style={bottomStyle}>
      <a href={thisServer} className="link-provider">deglet.xyz</a>
    </div>
  );

  return <div>{content} {bottom}</div>;
}


module.exports = template;
