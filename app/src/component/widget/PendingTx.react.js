"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var WalletAction = require('../../action/WalletAction');
var template = require('../../../template/widget/PendingTx.jsx');


var PendingTx = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    info: React.PropTypes.arrayOf(React.PropTypes.object),
    client: React.PropTypes.object
  },

  getInitialState: function() {
    return {
      waiting: false
    };
  },

  componentWillReceiveProps: function(props) {
    if (props && this.props.info) {
      if (props.length !== this.props.info.length) {
        /* Confirm / Cancel completed now. */
        this.setState({waiting: false});
      }
    }
  },

  _rejectTx: function(txid, event) {
    event.preventDefault();

    var txp;
    var client = this.props.client;

    if (!client) {
      return;
    }

    txp = {id: txid};
    this.setState({waiting: true}, function() {
      WalletAction.cancelTx(client, txp);
    });
  },

  _cosignRequest: function(txid, event) {
    event.preventDefault();

    var client = this.props.client;

    if (!client) {
      return;
    }

    this.setState({waiting: true}, function() {
      WalletAction.cosignRequest(client, txid);
    });
  },

  render: template

});


module.exports = PendingTx;
