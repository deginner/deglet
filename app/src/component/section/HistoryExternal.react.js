"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var WalletAction = require('../../action/WalletAction');
var HistoryExternalStore = require('../../store/HistoryExternalStore');
var template = require('../../../template/section/HistoryExternal.jsx');


var HistoryExternal = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    var data = HistoryExternalStore.getData();
    return {
      store: data,
      loading: data.history === null ? true : false,
      exporting: false,
      showFilter: false
    };
  },

  componentDidMount: function() {
    HistoryExternalStore.addChangeListener(this._onChange);

    if (this.state.loading) {
      this._getHistory();
    }
  },

  componentWillUnmount: function() {
    HistoryExternalStore.removeChangeListener(this._onChange);
  },

  _getHistory: function(pageNum) {
    this.setState({loading: true}, function() {
     WalletAction.historyExternal(pageNum || 1);
    });
  },

  _onChange: function() {
    var data = HistoryExternalStore.getData();
    this.setState({
      store: data,
      loading: false
    });
  },

  _onToggleFilter: function(event) {
    /* Show/Hide filter controls. */
    event.preventDefault();
    var showFilter = this.state.showFilter;
    this.setState({showFilter: !showFilter});
  },

  _onPaginate: function(page) {
    if (this.state.loading) {
      return;
    }

    var selected = page.selected + 1;
    var paginated = HistoryExternalStore.setCurrPage(selected);
    if (paginated === false) {
      /* Page is not available in the client, load from the server. */
      this._getHistory(selected);
    }
  },

  _onClearFilters: function(event) {
    event.preventDefault();
    if (this.state.loading) {
      return;
    }

    if (this.refs.tablefilter) {
        this.refs.tablefilter.clear();
    }

    if (!HistoryExternalStore.clearFilters()) {
      /* Need to fetch data from the server. */
      this._getHistory();
    }
  },

  _onApplyFilter: function(match) {
    if (this.state.loading) {
      return;
    }

    if (match.txid) {
      /* In this case, txid is actually id and it is case insensitive. */
      match.id = match.txid.toUpperCase();
      delete match.txid;
    }

    if (match.address) {
      match.payment_address = match.address; //eslint-disable-line camelcase
      delete match.address;
    }

    var filtered = HistoryExternalStore.filter(match);
    if (filtered === false) {
      /* Filter in the server. */
      this._getHistory();
    }
  },

  render: template

});


module.exports = HistoryExternal;
