"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var WalletAction = require('../../action/WalletAction');
var WalletHistoryStore = require('../../store/WalletHistoryStore');
var template = require('../../../template/section/HistoryBitcoin.jsx');


var tableSort = {

  /* Sortable columns. */
  column: {
    /* Keys are the labels present in the table shown to the user. */
    when: {
      /* "name" is the field name in the actual data. */
      name: "time",
      type: "number"
    },
    amount: {
      name: "amount",
      type: "number"
    },
    fees: {
      name: "fees",
      type: "number"
    }
  },

  /* Default icon. */
  nosort: "icon-sort",

  /* Entry types. */
  number: {
    desc: "icon-sort-number-down",
    asc: "icon-sort-number-up"
  },
  name: {
    desc: "icon-sort-name-down",
    asc: "icon-sort-name-up"
  }

};


var History = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    var data = WalletHistoryStore.getData();
    return {
      store: data,
      loading: data.history === null ? true : false,
      exporting: false,
      showFilter: false
    };
  },

  componentDidMount: function() {
    WalletHistoryStore.addChangeListener(this._onChange);
    WalletHistoryStore.addDownloadListener(this._onDownload);

    if (this.props.client && this.state.loading) {
      this._getHistory(this.props.client);
    }
  },

  componentWillReceiveProps: function(props) {
    if (props.client && !this.props.client) {
      /* Got a client now. */
      var client = props.client;
      this._getHistory(client);
    }
  },

  componentWillUnmount: function() {
    WalletHistoryStore.removeChangeListener(this._onChange);
    WalletHistoryStore.removeDownloadListener(this._onDownload);
  },

  _onChange: function() {
    var data = WalletHistoryStore.getData();
    this.setState({
      store: data,
      loading: false
    });
  },

  _onDownload: function(filepath) {
    this.setState({exporting: false}, function() {
      console.log('download file', filepath);
      if (filepath) {
        /* Got a url to the resulting file, download it now. */
        var link = React.findDOMNode(this.refs.export);
        link.href = filepath;
        link.click();
      }
    });
  },

  _toggleFilter: function(event) {
    /* Show/Hide filter controls. */
    event.preventDefault();
    var showFilter = this.state.showFilter;
    this.setState({showFilter: !showFilter});
  },

  _onPaginate: function(page) {
    if (this.state.loading) {
      return;
    }

    var selected = page.selected + 1;
    var paginated = WalletHistoryStore.setCurrPage(selected);

    if (paginated === false) {
      /* Page is not available in the client, load from the server. */
      this._getHistory(this.props.client, selected);
    }
  },

  _clearFilters: function(event) {
    event.preventDefault();
    if (this.state.loading) {
      return;
    }

    if (this.refs.tablefilter) {
      this.refs.tablefilter.clear();
    }

    if (!WalletHistoryStore.clearFilters()) {
      /* Need to fetch data from the server. */
      this._getHistory(this.props.client);
    }
  },

  _applyFilter: function(match) {
    if (this.state.loading) {
      return;
    }

    if (match.address) {
      match.addressTo = match.address;
      delete match.address;
    }

    var filtered = WalletHistoryStore.filter(match);
    if (filtered === false) {
      /* Filter in the server. */
      this._getHistory(this.props.client);
    }
  },

  _onTableSort: function(column) {
    if (this.state.loading) {
      return;
    }

    if (!tableSort.column[column]) {
      /* Column is not configured to be sorted. */
      return;
    }

    var name = tableSort.column[column].name;
    if (WalletHistoryStore.sort(name) === false) {
      /* Sort in the server. */
      this._getHistory(this.props.client, this.state.store.currPage);
    }
  },

  _onExport: function() {
    if (!this.props.client) {
      return;
    }
    this.setState({exporting: true});
    var columns = [
      "txid", "time", "action",
      "amount", "fees", "confirmations"
    ];
    WalletAction.historyDownload(this.props.client, columns);
  },

  _getHistory: function(client, pageNum) {
    this.setState({loading: true}, function() {
      WalletAction.history(client, pageNum || 1);
    });
  },

  _tableIcon: function(column) {
    /* Return the class names for a given column to use
     * and display sort icons.
     */

    var entry = tableSort.column[column];
    if (!entry || !tableSort[entry.type]) {
      /* Column is not configured to take sort icons. */
      return null;
    }

    var name = entry.name;
    var sortOrder = this.state.store.sort[name];
    var current = tableSort.nosort;

    if (typeof sortOrder === 'number' && sortOrder !== 0) {
      /* Map sort order to ascending (1) / descending (-1). */
      var field = (sortOrder === -1) ? "desc" : "asc";
      current = tableSort[entry.type][field];
    } else if (entry.start) {
      current = tableSort[entry.type][entry.start];
    }

    return "icon " + current;
  },

  render: template

});


module.exports = History;
