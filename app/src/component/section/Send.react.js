"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var WalletAction = require('../../action/WalletAction');
var WalletSendAction = require('../../action/WalletSendAction');
var WalletSendStore = require('../../store/WalletSendStore');
var debug = require('../../debug')(__filename);
var template = require('../../../template/section/Send.jsx');


var Send = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  getInitialState: function() {
    return {
      submitted: false,
      info: '',
      err: ''
    };
  },

  componentDidMount: function() {
    debug('mounted');
    WalletSendStore.addChangeListener(this._onChange);

    var width = window.innerWidth || 1000;
    if (width >= 768) {
      React.findDOMNode(this.refs.address).focus();
    }
  },

  componentWillUnmount: function() {
    WalletSendStore.removeChangeListener(this._onChange);
  },

  _hideMessage: function() {
    this.setState({err: '', info: ''});
  },

  _onChange: function() {
    var sendInfo = WalletSendStore.getData();
    this.setState({
      submitted: false,
      err: sendInfo.error ? sendInfo.msg : "",  /* XXX this gets confusing */
      info: sendInfo.error ? "" : sendInfo.msg  /* XXX this gets confusing */
    });

    if (!sendInfo.error) {
      /* Transaction has been submitted successfully,
       * update wallet status. */
      WalletAction.status(this.props.wallet.client);
      /* Clear the "amount" input to avoid resubmitting by mistake. */
      var amountNode = React.findDOMNode(this.refs.amount);
      if (amountNode) {
        amountNode.value = '';
      }
    }
  },

  _onSubmit: function(event) {
    event.preventDefault();
    var client = this.props.wallet.client;
    if (!client) {
      this.setState({err: 'error.no_wallet'});
      return;
    } else if (!this.props.wallet.ready) {
      this.setState({err: 'error.wallet_not_ready'});
      return;
    }

    var address = React.findDOMNode(this.refs.address).value;
    var amount = React.findDOMNode(this.refs.amount).value;
    /* Convert amount to satoshis. */
    var satoshi = Math.round(amount * 1e8);

    if (satoshi <= 0) {
      this.setState({err: 'error.amount_too_low'});
      return;
    }

    this.setState({submitted: true, err: '', info: ''}, function() {
      WalletSendAction.submit(client, address, satoshi);
    });
  },

  render: template

});


module.exports = Send;
