"use strict";

var React = require('react');
var template = require('../../template/Notification.jsx');


var Notification = React.createClass({

  propTypes: {
    info: React.PropTypes.shape({
      type: React.PropTypes.string.isRequired,
      data: React.PropTypes.object
    })
  },

  render: template

});


module.exports = Notification;
