"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');


var _notification = {
  type: '',
  data: null
};


var WalletNotificationStore = BaseStore.create();


WalletNotificationStore.getData = function() {
  return _notification;
};


WalletNotificationStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.WALLET_NOTIF_IN_TX: {
      /* Incoming transaction. */
      _notification = {
        type: 'intx',
        data: action.data
      };
      WalletNotificationStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_NOTIF_OUT_TX: {
      /* Outgoing transaction. */
      _notification = {
        type: 'outtx',
        data: action.data
      };
      WalletNotificationStore.emitChange();
      break;
    }

    /* XXX cleanup on logout */
  }

});


module.exports = WalletNotificationStore;
