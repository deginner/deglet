"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var ErrorKey = require('../util/errorkey');
var debug = require('../debug')(__filename);


var _sendInfo = {
  msg: '',
  error: null,
  id: null
};


var WalletSendStore = BaseStore.create();


WalletSendStore.getData = function() {
  return _sendInfo;
};


WalletSendStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.SEND_OK: {
      debug('accepted tx', action);
      _sendInfo.msg = 'send.success';
      _sendInfo.error = false;
      _sendInfo.id = action.id;
      WalletSendStore.emitChange();
      break;
    }

    case ActionTypes.SEND_FAIL: {
      debug('rejected tx', action);
      var errkey = ErrorKey.bws(action);

      _sendInfo.msg = errkey;
      _sendInfo.error = true;
      _sendInfo.id = null;
      WalletSendStore.emitChange();
      break;
    }

  }

});


module.exports = WalletSendStore;
