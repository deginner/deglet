"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var ActionTypes = require('../constant/ActionTypes');
var BaseStore = require('./BaseStore');
var errorkey = require('../util/errorkey');
var request = require('../util/request');
var debug = require('../debug')(__filename);

var _invoice;


function _emptyInvoice() {
  return {
    obj: null,
    error: null
  };
}

_invoice = _emptyInvoice();


var InvoiceStore = assign({}, EventEmitter.prototype, {

  getData: function() {
    return _invoice;
  },

  clear: function() {
    _invoice = _emptyInvoice();
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  },

  emitRefresh: function() {
    this.emit(EventConstants.REFRESH_EVENT);
  },

  addRefreshListener: function(callback) {
    this.on(EventConstants.REFRESH_EVENT, callback);
  },

  removeRefreshListener: function(callback) {
    this.removeListener(EventConstants.REFRESH_EVENT, callback);
  }

});


function invoiceSuccess(data) {
  if (data.error) {
    /* Status 200 but contains a validation error. */
    _invoice.error = errorkey.deglet(data);
  } else {
    _invoice.obj = data;
    _invoice.error = null;
  }
  InvoiceStore.emitChange();
}


function invoiceSuccessRefresh(data) {
  _invoice.obj = data;
  InvoiceStore.emitRefresh();
}


function invoiceError(err, info) {
  debug("request.invoice failed: " + err, info);

  var key = BaseStore.parseDegletError(info);
  if (key === null) {
    key = 'error.deglet.trans_fetch';
  }
  _invoice.error = key;

  InvoiceStore.emitChange();
}


InvoiceStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    case ActionTypes.INVOICE_LOAD: {
      var iid = action.id;
      var method;

      if (action.method !== 'get' && action.method !== 'post') {
        _invoice.error = 'Unknown load method';
        InvoiceStore.emitChange();
        return;
      }

      method = request[action.method];
      method('/invoice/' + iid, null, invoiceSuccess, invoiceError);
      break;
    }

    case ActionTypes.INVOICE_CREATE: {
      request.post('/invoice', action.data, invoiceSuccess, invoiceError);
      break;
    }

    case ActionTypes.INVOICE_REFRESH: {
      request.get(
        '/invoice/' + action.id, null,
        invoiceSuccessRefresh, function(){}
      );
      break;
    }

  }

});


module.exports = InvoiceStore;
