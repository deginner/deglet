/* Store data using sessionStorage. */
"use strict";

var store = require('store2');
var login = store.namespace('login');


/* XXX This could be using login.session.getAll() / login.session.setAll()
 * to store data in sessionStorage (otherwise this is not actually a
 * temporary storage). To use that, at a minimum a dialog
 * asking for user credentials need to be added.
 */

function get() {
  return login.getAll();
}

function erase() {
  login.clearAll();
}

function save(data) {
  login.setAll(data);
}


module.exports = {
  get: get,
  save: save,
  erase: erase
};
