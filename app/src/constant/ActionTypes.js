var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({

  CONVERT_CREATE: null,

  INVOICE_LOAD: null,
  INVOICE_REFRESH: null,
  INVOICE_CREATE: null,

  LOCALE_CHANGE: null,

  LOGIN_OK: null,
  LOGIN_STATUS: null,
  LOGOUT: null,

  SEND_OK: null,
  SEND_FAIL: null,
  COSIGN_OK: null,
  COSIGN_FAIL: null,

  WALLET_ERROR: null,
  WALLET_CREATED: null,
  WALLET_LISTENING: null,
  WALLET_CONNECTED: null,
  WALLET_DISCONNECTED: null,
  WALLET_OPEN: null,
  WALLET_STATUS: null,
  WALLET_BALANCE: null,
  WALLET_NEW_ADDRESS: null,
  WALLET_HISTORY_FETCH: null,
  WALLET_ADDRESSES: null,
  WALLET_ADDRESS_INFO: null,
  WALLET_NOTIF_IN_TX: null,
  WALLET_NOTIF_OUT_TX: null,
  BLOB_STORE: null,
  TXINFO: null,
  HISTORY_EXTERNAL_FETCH: null,
  CONVERSION_EXTERNAL_PAY: null,
  ADDRESS_EXTERNAL_INFO: null,

  TICKER: null,

  FIAT_BALANCE: null,
  RAW_FIAT_BALANCE: null

});
