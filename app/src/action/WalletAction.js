"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');
var LoginStore = require('../store/LoginStore');  /* Only for reading. */
var request = require('../util/request');
var sjcl = require('../util/sjcl');


function walletError(action, info) {
  return {
    actionType: ActionTypes.WALLET_ERROR,
    error: {
      name: action,
      reason: info
    }
  };
}


function walletData(action, data) {
  return {
    actionType: action,
    data: data
  };
}


function walletDispatch(action, err, data) {
  if (typeof action !== 'string') {
    throw new Error('unknown wallet action "' + action + '"');
  }

  var event;

  if (!err) {
    event = walletData(action, data);
  } else {
    event = walletError(action, err);
  }

  AppDispatcher.dispatch(event);
}


function walletNotification(action, data) {
  if (typeof action !== 'string') {
    throw new Error('unknown notification action "' + action + '"');
  }

  AppDispatcher.dispatch({
    actionType: action,
    data: data
  });
}


function _remoteStore(loginData, blob, secret, callback, errback) {
  if (!loginData.uid || !loginData.key) {
    throw new Error("credentials not present");
  }
  var uid = loginData.uid;
  var encBlob = sjcl.encrypt(loginData.key, blob);

  var data = {blob: encBlob};
  if (secret) {
    /* This secret is passed to the cosigner so it can join the wallet. */
    data.secret = secret;
  }

  request.put(
    '/' + uid,
    JSON.stringify(data),
    function(obj) {
      /* Stored remotely! Proceed. */
      if (obj[uid] !== encBlob) {
        errback({err: "server did not echo the data"});
        return;
      }
      console.log('request.put result', obj);
      /* Send event to cause it to be stored locally. */
      AppDispatcher.dispatch({
        actionType: ActionTypes.BLOB_STORE,
        blob: encBlob
      });
      callback();
    },
    errback
  );
}


var WalletAction = {

  startListening: function(client) {
    var uid = LoginStore.getData().uid;

    client.on('notification', function(data) {
      WalletAction.notification(client, data);
    });

    client.on('reconnecting', function() {
      console.log('reconnecting..');
      WalletAction.disconnected();
    });


    client.on('deglet-ticker', function(ticker) {
      /* Got a new ticker. */
      AppDispatcher.dispatch({
        actionType: ActionTypes.TICKER,
        ticker: ticker
      });
    });

    client.on('deglet-fiat-balance', function(balance) {
      AppDispatcher.dispatch({
        actionType: ActionTypes.FIAT_BALANCE,
        data: balance
      });
    });

    client.on('deglet-transaction', function(update) {
      console.log('deglet transaction update', update);
    });

    client.on('deglet-connected', function(socket) {
      socket.emit('join', 'ticker');
      socket.emit('join', 'fiat-balance', uid);
      socket.emit('join', 'transaction', uid);
      request.get(
        '/' + uid, null,
        function(data) {
          AppDispatcher.dispatch({
            actionType: ActionTypes.RAW_FIAT_BALANCE,
            data: data
          });
        },
        function(err, info) {
          /* XXX */
          console.log('failed to load user data', err, info);
        }
      );
    });


    client.initNotifications(function(err) {
      /* This is called every time the client connects to the BWS server. */
      if (err) {
        console.log('error while initializing notifications:', err);
        /* XXX Dispatch event for "wallet-failed-to-start-listening"? */
      } else {
        AppDispatcher.dispatch({actionType: ActionTypes.WALLET_LISTENING});
        WalletAction.status(client);
        console.log('wallet complete?', client.isComplete());
        if (client.isComplete()) {
          walletDispatch(ActionTypes.WALLET_OPEN, null, null);
        } else {
          /* This wallet might be complete but the client is unaware because
           * the dedicated cosigner completed it before the socket-io connection
           * was stablished. Due to that that, call now openWallet to make sure
           * it is in sync.
           */
          WalletAction.open(client);
        }
      }
    });
  },

  connected: function() {
    AppDispatcher.dispatch({actionType: ActionTypes.WALLET_CONNECTED});
  },

  disconnected: function() {
    AppDispatcher.dispatch({actionType: ActionTypes.WALLET_DISCONNECTED});
  },

  create: function(client, loginData) {
    if (!loginData.key) {
      console.log('creds missing!');
      return;
    }

    /* Create a new wallet. */
    var walletName = "wallet" + sjcl.randomHex(2);
    var userName = "user" + sjcl.randomHex(2);
    var opt = {network: "testnet"}; /* XXX move constants to constant/ */
    client.createWallet(walletName, userName, 2, 2, opt, function(err, secret) {
      if (err) {
        console.log('error while creating wallet', err);
        /* XXX show error to the client. */
        return;
      }

      /* Send both the secret for joining this wallet and the
       * user's encrypted wallet to the server.
       */
      _remoteStore(loginData, client.export(), secret,
        function() {
          /* Let store(s) know that the wallet has been created. */
          walletDispatch(ActionTypes.WALLET_CREATED, null, client);
        },
        function(errSave, res) {
          /* XXX dispatch event and show to the user. */
          console.log('failed to save/receive data :/', errSave, res);
        }
      );
    });
  },

  addresses: function(client) {
    client.getMainAddresses({}, function(err, list) {
      walletDispatch(ActionTypes.WALLET_ADDRESSES, err, list);
    });
  },

  newAddress: function(client) {
    client.createAddress(function(err, data) {
      walletDispatch(ActionTypes.WALLET_NEW_ADDRESS, err, data);
    });
  },

  fetchAddress: function(client, address) {
    client.getAddress({address: address}, function(err, data) {
      if (err || data.walletId) {
        /* Got an error, or an address that belongs to this account was found. */
        walletDispatch(ActionTypes.WALLET_ADDRESS_INFO, err, data);
      } else {
        /* Lookup for an address provided by the broker. */
        var uid = LoginStore.getData().uid;
        var req = {
          address: address,
          auth: {
            user: uid,
            password: ''
          }
        };
        walletDispatch(ActionTypes.ADDRESS_EXTERNAL_INFO, null, req);
      }
    });
  },

  balance: function(client) {
    client.getBalance(function(err, bal) {
      walletDispatch(ActionTypes.WALLET_BALANCE, err, bal);
    });
  },

  status: function(client) {
    client.getStatus(function(err, info) {
      walletDispatch(ActionTypes.WALLET_STATUS, err, info);
    });
  },

  txInfo: function(txid) {
    request.getBlockchainTx(
      txid,
      function(data) {
        walletDispatch(ActionTypes.TXINFO, null, data);
      },
      function(err, info) {
        console.log('txInfo failed', err, info);
        walletDispatch(ActionTypes.TXINFO, err, null);
      }
    );
  },

  cancelTx: function(client, txp) {
    client.removeTxProposal(txp, function(err) {
      if (!err) {
        console.log('proposal removed');
        WalletAction.status(client);
      } else {
        console.log('error while trying to reject transaction:', err);
      }
    });
  },

  cosignRequest: function(client, txid) {
    var uid = LoginStore.getData().uid;
    request.post(
      '/' + uid + '/' + txid,
      JSON.stringify({}),
      function(obj) {
        /* Cosigned! */
        WalletAction.status(client);
        walletDispatch(ActionTypes.COSIGN_OK, null, obj);
      },
      function(err) {
        /* Cosigning failed. */
        walletDispatch(ActionTypes.COSIGN_FAIL, err, null);
      }
    );
  },

  open: function(client) {
    client.openWallet(function(err, data) {
      if (!client.credentials.isComplete()) {
        return;
      }

      /* Remotely store updated and now complete wallet. */
      var loginData = LoginStore.getData();
      _remoteStore(loginData, client.export(), null,
        function() {
          /* Let store(s) know that the wallet has been stored remotely. */
          walletDispatch(ActionTypes.WALLET_OPEN, err, data);
          /* Get an initial address. */
          WalletAction.newAddress(client);
        },
        function(errSave, res) {
          /* XXX dispatch event and show to the user. */
          console.log('failed to save/receive data :/', errSave, res);
        }
      );
    });
  },

  history: function(client, firstPage) {
    var data = {client: client, firstPage: firstPage};
    walletDispatch(ActionTypes.WALLET_HISTORY_FETCH, null, data);
  },

  historyDownload: function(client, columns) {
    var data = {client: client, firstPage: 1, download: columns};
    walletDispatch(ActionTypes.WALLET_HISTORY_FETCH, null, data);
  },

  historyExternal: function(firstPage) {
    var uid = LoginStore.getData().uid;
    var data = {
      firstPage: firstPage,
      auth: {
        user: uid,
        password: ''
      }
    };
    walletDispatch(ActionTypes.HISTORY_EXTERNAL_FETCH, null, data);
  },

  payConversion: function(transId) {
    var uid = LoginStore.getData().uid;
    var data = {
      transId: transId,
      auth: {
        user: uid,
        password: ''
      }
    };
    walletDispatch(ActionTypes.CONVERSION_EXTERNAL_PAY, null, data);
  },

  notification: function(client, notif) {
    console.log('got notification:', notif);
    switch (notif.type) {

      case "NewIncomingTx": {
        walletNotification(ActionTypes.WALLET_NOTIF_IN_TX, notif.data);
        /* Update balance. */
        WalletAction.balance(client);
        /* Update history. XXX This should be done in a more efficient way. */
        WalletAction.history(client, 1);
        break;
      }

      case "NewOutgoingTx": {
        /* Outgoing transaction fully signed and broadcasted. */
        walletNotification(ActionTypes.WALLET_NOTIF_OUT_TX, notif.data);
        /* Update balance information and also the list of
         * pending transactions. */
        /* XXX Calling WalletAction.status right way does not
         * always result in up to date balances (even with
         * this delay it does not guarantee it). */
        setTimeout(function() {
          WalletAction.status(client);
          WalletAction.history(client, 1);
        }, 250);
        break;
      }

      case "WalletComplete": {
        /* External cosigner completed its part. */
        WalletAction.open(client);
        break;
      }

    }
  }

};


module.exports = WalletAction;
