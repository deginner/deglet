"use strict";

var AppDispatcher = require('../dispatcher');
var ActionTypes = require('../constant/ActionTypes');
var debug = require('../debug')(__filename);


var WalletSendAction = {

  submit: function(client, address, amount) {
    /* Submit transaction proposal. */
    debug('create a transaction', client, address, amount);
    var options = {
      toAddress: address,
      amount: amount
    };
    client.sendTxProposal(options, function(err, tx) {
      if (!err) {
        WalletSendAction.proposed(client, tx);
      } else {
        WalletSendAction.proposalFail(err);
      }
    });
  },

  proposed: function(client, tx) {
    /* Transaction proposal accepted, now sign it. */
    debug('proposal worked!', tx);
    client.signTxProposal(tx, function(err, res) {
      if (!err) {
        WalletSendAction.signed(client, res);
      } else {
        WalletSendAction.signFail(err);
      }
    });
  },

  signed: function(client, data) {
    /* Transaction proposal signed. */
    debug('tx signed', data);
    data.actionType = ActionTypes.SEND_OK;
    AppDispatcher.dispatch(data);
  },

  proposalFail: function(data) {
    debug('proposal failed!', data);
    data.actionType = ActionTypes.SEND_FAIL;
    AppDispatcher.dispatch(data);
  },

  signFail: function(data) {
    debug('signing failed!', data);
    /* XXX piggyback on proposalFail actionType */
    data.actionType = ActionTypes.SEND_FAIL;
    AppDispatcher.dispatcher(data);
  }

};


module.exports = WalletSendAction;
