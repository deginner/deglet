var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');
var data = require('./data');


test.describe('Deglet - visit user links', function() {

  test.it('shoud login successfully', function(done) {
    base.login(this.driver, base, data.user, done);
  });

  test.it('should display the initial wallet page', function(done) {
    expect('.wallet-address input').dom.to.be.visible();
    expect('.balance tbody tr').dom.to.be.visible();
    expect('.pendingtx table').dom.to.be.visible();

    done();
  });

  test.it('should move to the Send page', function(done) {
    base.moveTo(this.driver, '/send', 'Send', done);
  });

  test.it('should submit an invalid form in the Send page', function(done) {
    expect('input[placeholder="Address"]').dom.to.be.visible();
    expect('button[type=submit]').dom.to.be.visible();
    base.fill(this.driver, {'address': '123', 'amount': '0.01'});
    base.submit(this.driver);
    expect('.error-box').dom.to.be.visible();
    done();
  });

  test.it('should move to the Receive page', function(done) {
    base.moveTo(this.driver, '/receive', 'Receive', done);
    expect('.user-tabs a[class$=active]').dom.to.have.text('Simple receive');
    expect('form input[readonly]').dom.to.be.visible();
    expect('.qr-container').dom.to.be.visible();
    done();
  });

  test.it('should move to the Invoice tab inside the Receive page', function(done) {
    base.moveToTab(this.driver, 'recvinvoice', 'Create invoice', done);
    expect('.select-dropdown').dom.to.be.visible();
    expect('.ladda-label').dom.to.have.text('Generate invoice');
    done();
  });

  test.it('should move to the Convert page', function(done) {
    base.moveTo(this.driver, '/convert', 'Convert', done);
    expect('.button[title="Swap currencies"]').dom.to.be.visible();
    expect('.button[title$="minutes to confirm it."]').dom.to.be.visible();
    done();
  });

  test.it('should move to the History page', function(done) {
    base.moveTo(this.driver, '/history', 'History', done);
    expect('.user-table').dom.to.be.visible();
    done();
  });

  test.it('should move to the Invoices & Conversions tab inside the History page', function(done) {
    base.moveToTab(this.driver, 'external', 'Invoices & Conversions', done);
  });

  test.it('should log out', function(done) {
    var driver = this.driver;
    driver.findElement({css: '.menu-links a[href$="/logout"]'}).click();
    driver.wait(function() {
      return driver.isElementPresent({css: '.loginarea form'});
    });
    expect(driver.getCurrentUrl()).to.eventually.equal(base.baseUrl + 'login');
    done();
  });

});
